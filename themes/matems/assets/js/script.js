(function($) {
	$(document).ready(function() {

		// Smooth scroll
		if($('#HomePage').length) {

		    //Smooth scroll for links to anchor
			$('a[href*="#"]:not(.carousel-controls, .collapse-toggle, .popbox, .puzzle, .puzzlepiece, #menubutton, .btn-sm)').click(function(e){
				e.preventDefault();

				var dest=0;
				if($(this.hash).offset().top > $(document).height()-$(window).height()) {
					dest=$(document).height()-$(window).height();
				} else{
					dest=$(this.hash).offset().top;
				}
				dest -= 0;
				$('html,body').animate({scrollTop:dest}, 1000,'swing');
			});
		}

		// slideout menu
		var slideout = new Slideout({
			'panel': document.getElementById('panel'),
			'menu': document.getElementById('menu'),
			'side': 'right',
  			'touch': false
		});
		// Toggle button
		document.querySelector('#menubutton').addEventListener('click', function(e) {
			e.preventDefault();
			slideout.toggle();
		});

		var e=false;
		var t=0;
		var n=$(window).width();
		var t=$(window).height();
		var r=[];
		$("#menu nav ul li a").on("click",function(e){
			e.preventDefault();
			slideout.close();
			var t=this.hash,n=$(t);
			$("html, body").stop().delay(300).animate({scrollTop:n.offset().top-0},900,"swing",function(){window.location.hash=t})
		});

		// placeholder fallback ie9
		$('.UserDefinedForm input, .UserDefinedForm textarea').placeholder();

        // popover
        /**
        * Add the equals method to the jquery objects
        */
        $.fn.equals = function(compareTo) {
            if (!compareTo || this.length !== compareTo.length) return false;
            for (var i = 0; i < this.length; ++i) {
                if (this[i] !== compareTo[i]) return false;
            }
            return true;
        };

        /**
        * Activate popover message for all concerned fields
        */
        var popoverOpened = null;
        $(function() {
            $('.popbox').popover();
            $('.popbox').unbind("click");
            $('.popbox').bind("click", function(e) {
                e.stopPropagation();
                if($(this).equals(popoverOpened)) {
                    popoverOpened.popover("hide");
                    return;
                }
                if(popoverOpened !== null) {
                    popoverOpened.popover("hide");
                }
                $(this).popover('show');
                popoverOpened = $(this);
                e.preventDefault();
            });

            $(document).click(function(e) {
                if(popoverOpened !== null) {
                    popoverOpened.popover("hide");
                    popoverOpened = null;
                }
            });
        });

        $('.popbox').on("click", function(e) {
            e.preventDefault();
        });


        $('body').popover({ selector: '.puzzlepiece', trigger: 'click hover'});
        $('.puzzlepiece').on('click', function(e) {
            e.preventDefault();
        });

        $('.Team .collapse-toggle').click(function(e) {
            e.preventDefault();
            $(this).closest('.Team').addClass('no-border');
            $('.Team .collapse-toggle').removeClass('active');
            $(this).addClass('active');
            $('.Team .collapse').hide();
            $('.Team .collapse-toggle').removeClass('active');
            $(this).addClass('active').closest('div');
            $(this).closest('div').children('.collapse').show();


            var elementT = $(this.hash).offset().top;
            var elementB = elementT + $(this.hash).outerHeight();
            var windowP = $(window).scrollTop();
            var windowH = $(window).height();
            var viewportB = windowH + windowP;
            var pxToScroll = windowP - (viewportB - elementB);

            //$('html,body').animate({scrollTop:pxToScroll}, 200,'swing');
        });

        function closeReveal() {
            $('.Team').removeClass('no-border');
            $('.Team .collapse-toggle').removeClass('active');
            $('.Team .collapse').hide(500);
        }
        $('.Team .collapse .close').click(function(e) {
            closeReveal();
        });
	});
})(jQuery);