<?php

class Offering extends Page {
    private static $singular_name = "Offering";
    private static $description = "Section about the offering";
    private static $allowed_children = "none";

    private static $has_one = array(
        "BackgroundImage" => "Image"
    );

    private static $has_many = array(
        "Offers" => "Offer"
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeFieldFromTab("Root.Main","Content");
        $fields->addFieldToTab('Root.Main', new UploadField('BackgroundImage', 'Background Image'));
        $conf=GridFieldConfig_RelationEditor::create(10);
        $conf->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab("Root.Main", new GridField('Offers', 'Offering', $this->Offers(), $conf));
        return $fields;
    }
}

class Offering_Controller extends Page_Controller {}