<?php

class Clients extends Page {
    private static $singular_name = "Clients";
    private static $description = "Section about the Clients";
    private static $allowed_children = "none";


    private static $many_many = array(
        'Images' => 'Image'
    );

    // this adds the SortOrder field to the relation table.
    // Please note that the key (in this case 'Images')
    // has to be the same key as in the $many_many definition!
    private static $many_many_extraFields = array(
        'Images' => array('SortOrder' => 'Int')
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Use SortableUploadField instead of UploadField!
        $imageField = new SortableUploadField('Images', "Client logo's");
        $fields->addFieldToTab('Root.Main', $imageField);
        return $fields;
    }

    // Use this in your templates to get the correctly sorted images
    // OR use $Images.Sort('SortOrder') in your templates which
    // will unclutter your PHP classes
    public function SortedImages(){
        return $this->Images()->Sort('SortOrder');
    }
}

class Clients_Controller extends Page_Controller {}