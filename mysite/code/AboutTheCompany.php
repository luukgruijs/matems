<?php

class AboutTheCompany extends Page {
    private static $singular_name = "About Matems";
    private static $description = "Section about Matems";
    private static $allowed_children = "none";

    private static $db = array(
        "Content2" => "HTMLText",
        "Content3" => "HTMLText",
        "Content4" => "HTMLText",
    );


    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', new HTMLEditorField('Content2', 'Vision'));
        $fields->addFieldToTab('Root.Main', new HTMLEditorField('Content3', 'Our culture'));
        $fields->addFieldToTab('Root.Main', new HTMLEditorField('Content4', 'Mission'));
        return $fields;
    }
}

class AboutTheCompany_Controller extends Page_Controller {}