<?php

class UserDefinedFormExtend extends DataExtension {
    private static $db = array(
        "Content2" => "HTMLText"
    );

    public function updateCMSFields(Fieldlist $fields) {
        $fields->addFieldToTab('Root.Main', new HTMLEditorField('Content2', 'Content2'));
        return $fields;
    }
}