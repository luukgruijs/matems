<?php

class SiteConfigExtend extends DataExtension {

    private static $db = array(
        "Twitter" => "Varchar(255)",
        "Facebook" => "Varchar(255)",
        "Youtube" => "Varchar(255)",
        "GooglePlus" => "Varchar(255)",
    );

    private static $has_one = array(
        "Logo" => "Image",
    );

    private static $many_many = array(
        "HeaderNav" => "Page"
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab(
            "Root.Main", array(
            TreeMultiSelectField::create('HeaderNav',_t('SiteConfig.HeaderNav', 'Navigatie'), 'SiteTree'),
        ));
        $fields->addFieldsToTab(_t('Root.Theme', "Root.Thema"), new UploadField('Logo', 'Logo'));
        $fields->addFieldsToTab(_t('Root.social', "Root.Social"), new TextField('Twitter', 'Link naar Twitter'));
        $fields->addFieldsToTab(_t('Root.social', "Root.Social"), new TextField('Facebook', 'Link naar Facebook'));
        $fields->addFieldsToTab(_t('Root.social', "Root.Social"), new TextField('Youtube', 'Link naar Youtube'));
        $fields->addFieldsToTab(_t('Root.social', "Root.Social"), new TextField('GooglePlus', 'Link naar GooglePlus'));
        return $fields;
    }

    // Return menu items
    public function HeaderNav(){
        return $this->HeaderNav('', 'SortOrder ASC');
    }
}