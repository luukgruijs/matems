<?php
class Page extends SiteTree {

	public function getCMSFields() {
		$fields = parent::getCMSFields();

	    if($this->ClassName == 'ErrorPage'
	  	|| $this->ClassName == 'BlogTree'
	  	|| $this->ClassName == 'RedirectorPage'
	  	|| $this->ClassName == 'VirtualPage'
	  	|| $this->ClassName == 'HomePage'
	  	|| $this->ClassName == 'TextPage'
	  	|| $this->ClassName == 'BlogHolder'
	  	|| $this->ClassName == 'BlogEntry'
	  	|| $this->ClassName == 'ContactPage'
	  	|| $this->ClassName == 'ServicePage' ) {
	    	// Tab for Metadata
			$fields->removeByName('Metadata');
			$tab = 'Zoekmachine';
			$fields->addFieldsToTab(
			 "Root.$tab", array(
			    TextField::create('MetaTitle','Title-tag')->setDescription('Dit is de titel van uw zoekresultaat in Google.'),
			    TextareaField::create('MetaDescription','Beschrijvende meta-tag')->setRows(15)->setDescription('Dit is de omschrijving van uw zoekresultaat in Google. Lees de <a href="http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.nl/en/nl/intl/nl/webmasters/docs/search-engine-optimization-starter-guide-nl.pdf" target="_blank">Beginnershandleiding voor<br/>zoekmachineoptimalisatie van Google</a> voor meer informatie over deze velden.')
			));
	  	} else {
	  		$fields->removeFieldsFromTab('Root.Main',
			 	array(
					'MetaTitle',
					'MetaDescription',
				)
			);
			$fields->removeByName('Metadata');
			$fields->removeByName("Zoekmachine");
		}
		return $fields;
	}

	// Show UserDefinedForm in other template
	function ContactForm(){
		$ContactForm = UserDefinedForm::get()->byID(10);
		if($ContactForm) return new UserDefinedForm_Controller($ContactForm);
	}

	function AboutTheCompanyLink() {
		return AboutTheCompany::get()->first()->URLSegment;
	}
}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();

		$base = Director::baseFolder();
	    $path = $this->ThemeDir();
	    $jquerypath = Director::protocol().'ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js';

	   	// Load JS
	    Requirements::block(FRAMEWORK_DIR .'/thirdparty/jquery/jquery.js'); // Prevent jQuery from being loaded twice
	    Requirements::javascript("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js");
	    Requirements::javascript($path . '/assets/js/vendor.min.js');
	    Requirements::javascript($path . '/assets/js/script.min.js');

	    // Load CSS
	  	Requirements::css('http://fonts.googleapis.com/css?family=Lato:300,900');
	 	Requirements::css($path . '/assets/css/vendor.min.css');
	 	Requirements::css($path . '/assets/css/style.css');
	}
}