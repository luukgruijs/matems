<?php

class TeamMember extends DataObject {

    private static $singular_name = "Team Member";
    private static $default_sort = 'SortOrder';

    private static $db = array(
        'Name' => 'varchar(255)',
        'Phone' => 'varchar(255)',
        'Email' => 'varchar(255)',
        'Description' => "HTMLText",
        'SortOrder' => 'Int'
    );

    private static $has_one = array (
        'Page' => 'Page',
        'Profile' => 'Image'
    );

    private static $summary_fields = array(
        'Name' => 'Name',
        'Phone' => 'Phone',
        'Email' => 'Email'
    );

    public function getCMSFields() {
        $fields = new FieldList();
        $fields->push(new UploadField('Profile', 'Profile image'));
        $fields->push(new TextField('Name', 'Name'));
        $fields->push(new TextField('Phone', 'Phone'));
        $fields->push(new TextField('Email', 'Email'));
        $fields->push(new HTMLEditorField('Description', 'Description'));
        return $fields;
    }
}

?>