<?php

class ProductCycle extends DataObject {

    private static $singular_name = "Product life cycle";
    private static $default_sort = 'SortOrder';

    private static $db = array(
        'Title' => 'varchar(255)',
        'Content' => 'HTMLText',
        'SortOrder' => 'Int'
    );

    private static $has_one = array (
        'Page' => 'Page',
        'Piece' => 'Image'
    );

    private static $summary_fields = array(
        'Title' => 'Title',
        'Content' => 'Content'
    );

    public function getCMSFields() {
        $fields = new FieldList();
        $fields->push(new UploadField('Piece', 'Piece'));
        $fields->push(new TextField('Title', 'Title'));
        $fields->push(new HTMLEditorField('Content', 'Content'));
        return $fields;
    }
}

class ProductCycle_Controller extends Page_Controller {}