<?php

class Team extends Page {
    private static $singular_name = "Team";
    private static $description = "Section about the Team";
    private static $allowed_children = "none";


    private static $has_many = array(
        "TeamMembers" => "TeamMember"
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $conf=GridFieldConfig_RelationEditor::create(10);
        $conf->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab("Root.Main", new GridField('TeamMembers', 'Team members', $this->TeamMembers(), $conf));
        return $fields;
    }
}

class Team_Controller extends Page_Controller {}