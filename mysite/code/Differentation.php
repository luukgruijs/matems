<?php

class Differentation extends Page {
    private static $singular_name = "What makes you different";
    private static $description = "Section about what makes matems different";

    private static $has_many = array(
        "ProductCycles" => "ProductCycle"
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $conf=GridFieldConfig_RelationEditor::create(10);
        $conf->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->addFieldToTab("Root.Main", new GridField('ProductCycles', 'Product life cycle', $this->ProductCycles(), $conf));
        return $fields;
    }
}