<?php

class HomePage extends Page {
    private static $singular_name = "Homepage";
    private static $description = "Main page of site";

    private static $has_one = array(
        "BackgroundImage" => "Image"
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->AddFieldsToTab('Root.Main', new UploadField('BackgroundImage', 'Background image'), 'Content');
        return $fields;
    }
}

class HomePage_Controller extends Page_Controller {}