<?php

global $project;
$project = 'mysite';

global $database;
$database = '';

require_once('conf/ConfigureFromEnv.php');

// Set the site locale
i18n::set_locale('en_GB');
setlocale(LC_TIME, 'en_GB.utf8');

// Default admin
if (Director::isDev()) {
    Security::setDefaultAdmin('admin', '123');
}

if (Director::isLive()) {
    Security::setDefaultAdmin('luuk@neoteric.digital', 'N3ot3ric.Digital');
}


/* TinyMCE Configuration */
$tinyMCE = HtmlEditorConfig::get('cms');
$tinyMCE->setOption('extended_valid_elements', '@[id|class|style|title],#a[id|rel|rev|dir|tabindex|accesskey|type|name|href|target|title|class],-strong/-b[class],-em/-i[class],-strike[class],-u[class],#p[id|dir|class|align|style],-ol[class],-ul[class],-li[class],br,img[id|dir|longdesc|usemap|class|src|border|alt=|title|width|height|align],-sub[class],-sup[class],-blockquote[dir|class],-table[border=0|cellspacing|cellpadding|width|height|class|align|summary|dir|id|style],-tr[id|dir|class|rowspan|width|height|align|valign|bgcolor|background|bordercolor|style],tbody[id|class|style],thead[id|class|style],tfoot[id|class|style],#td[id|dir|class|colspan|rowspan|width|height|align|valign|scope|style],-th[id|dir|class|colspan|rowspan|width|height|align|valign|scope|style],caption[id|dir|class],-div[id|dir|class|align|style],-span[class|align|style],-pre[class|align|name],address[class|align],-h1[id|dir|class|align|style],-h2[id|dir|class|align|style],-h3[id|dir|class|align|style],-h4[id|dir|class|align|style],-h5[id|dir|class|align|style],-h6[id|dir|class|align|style],hr[class],dd[id|class|title|dir],dl[id|class|title|dir],dt[id|class|title|dir],@[id,style,class]');
$tinyMCE->setOption('element_format', "html");
$tinyMCE->setOption('theme_advanced_resize_horizontal', 'true');
$tinyMCE->setOption('theme_advanced_resizing', 'true');

// Clean the HTML output
ob_start();
register_shutdown_function(function(){
    $buffer = ob_get_clean();
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    $buffer = preg_replace('/\n(\s*\n)+/', "\n", $buffer);
    $buffer = preg_replace('/\n(\s+)/', "\n", $buffer);
    echo $buffer;
});

// Useful Development configuration
if (Director::isDev()) {
    SS_Cache::set_cache_lifetime('default', -1, 100); // Disables the cache
}

if (Director::isLive()) {
    SS_Log::add_writer(new SS_LogEmailWriter('luuk@neoteric.digital'), SS_Log::ERR); // Sends to email instead of writing to a file
}

// bcc emails
Email::bcc_all_emails_to('luuk@neoteric.digital');

// SiteTree Settings
Config::inst()->update('SiteTree', 'nested_urls', true); // Enable/Disable the nested URL's

// Customise the Admin
LeftAndMain::setApplicationName("Matems Oncology");

