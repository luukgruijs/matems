var gulp    	= require('gulp'),
	gutil   	= require('gulp-util'),
	uglify  	= require('gulp-uglify'),
	less		= require('gulp-less'),
	concat  	= require('gulp-concat'),
	minifycss 	= require('gulp-minify-css')
	notify		= require('gulp-notify')
	livereload	= require('gulp-livereload');

gulp.task("bundle-css-plugins", function() {
	gulp.src("themes/matems/assets/css/vendor/*.css")
		.pipe(minifycss().on('error', gutil.log))
		.pipe(concat('vendor.min.css'))
		.pipe(gulp.dest("themes/matems/assets/css/"))
});

gulp.task("bundle-js-plugins", function() {
	gulp.src("themes/matems/assets/js/vendor/*.js")
		.pipe(uglify().on('error', gutil.log))
		.pipe(concat('vendor.min.js'))
		.pipe(gulp.dest("themes/matems/assets/js/"))
});

gulp.task("script", function() {
	gulp.src("themes/matems/assets/js/script.js")
	.pipe(uglify().on('error', gutil.log))
	.pipe(notify('Minified script'))
	.pipe(concat('script.min.js'))
	.pipe(gulp.dest("themes/matems/assets/js/"))
	.pipe(livereload())
});

gulp.task("less", function() {
	gulp.src("themes/matems/assets/less/style.less")
		.pipe(less({
			"compress": true
		}).on('error', gutil.log))
		.pipe(notify('Less Compiled'))
		.pipe(gulp.dest("themes/matems/assets/css/"))
		.pipe(minifycss().on('error', gutil.log))
		.pipe(livereload())
});

gulp.task('template', function() {
	gulp.src("themes/matems/templates/Layout/*.ss")
	.pipe(notify('templated edited'))
	.pipe(livereload())
});

gulp.task("watch", function() {
	livereload.listen();
	gulp.watch('themes/matems/assets/less/style.less', ['less']);
	gulp.watch('themes/matems/templates/Layout/*.ss', ['template']);
	gulp.watch('themes/matems/assets/js/script.js', ['script']);

	gulp.watch(['themes/matems/assets/less/style.less']).on('change', livereload.changed);
	gulp.watch(['themes/matems/templates/Layout/*.ss']).on('change', livereload.changed);
	gulp.watch(['themes/matems/assets/js/script.js']).on('change', livereload.changed);
})

gulp.task('default', ['watch','less']);