<?php

// Set database
define('SS_DATABASE_CLASS', 'MySQLDatabase');
define('SS_DATABASE_SERVER', 'localhost');
define('SS_DATABASE_USERNAME', 'root');
define('SS_DATABASE_PASSWORD', 'root');
define('SS_DATABASE_SUFFIX', 'matems_dev');

// Set environment type
define('SS_ENVIRONMENT_TYPE', 'dev');